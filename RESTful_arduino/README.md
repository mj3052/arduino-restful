Arduino RESTful HTTP
=========

* Digital pin I/O (HIGH, LOW and PWM)
* Analog pin input


To turn on the LED attached to pin #9 (case sensitive!):

http://192.168.1.177/9/HIGH

This will set the pin to the HIGH state and the LED should light.

http://192.168.1.177/100

This will use PWM to illuminate the LED at around 50% brightness (valid PWM values are 0-255).

Now if we connect a switch to pin #9 we can read the digital (on/off) value using this URL:

http://192.168.1.177/9

This returns a tiny chunk of JSON containing the pin requested and its current value:

{"9":"LOW"}

Analog reads are similar; reading the value of Analog pin #1 looks like this:

http://192.168.1.177/a1

...and return the same JSON formatted result as above:

{"a1":"432"}

